package com.arvore.projetoarvore.background;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.support.annotation.NonNull;

import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.arvore.projetoarvore.rede.Envia;
import com.arvore.projetoarvore.telasflow.Relatorio;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import static android.support.v4.content.ContextCompat.getExternalFilesDirs;
import static android.support.v4.content.ContextCompat.getSystemService;

public class TentaEnviar extends Worker {


    public TentaEnviar(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        String inputPathZip = getInputData().getString("inputPathZip");
        String finalZipName = getInputData().getString("finalZipName");
        String finalSentPath = getInputData().getString("finalSentPath");
        String thumbPath = getInputData().getString("thumbPath");
        String thumbName = getInputData().getString("thumbName");

        File teste = new File(thumbPath, thumbName);
        if (!teste.exists()) {
            return Result.success();
        }

        if(checkWifiOnAndConnected() == true){
            System.out.println("Enviando pela rede, agora assinc");
            System.out.println("");
            System.out.println("");
            System.out.println(inputPathZip + finalZipName);
            System.out.println("");
            envia_pela_rede_service(inputPathZip, finalZipName, finalSentPath, thumbPath, thumbName);
            return Result.success();
        }

        return null;
    }


    private boolean checkWifiOnAndConnected() {
        WifiManager wifiMgr = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        System.out.println("Esta entrando no checkWifiOnAndConnected");
        if (wifiMgr.isWifiEnabled()) { // Wi-Fi adapter is ON

            WifiInfo wifiInfo = wifiMgr.getConnectionInfo();

            if( wifiInfo.getNetworkId() == -1 ){
                return false; // Not connected to an access point
            }
            return true; // Connected to an access point
        }
        else {
            return false; // Wi-Fi adapter is OFF
        }
    }

    public static void envia_pela_rede_service(String inputPathZip, String finalZipName, String sentPath, String thumbPath, String thumbName) {
        Envia envia = new Envia();

            String test = inputPathZip + "/";
            envia.uploadZip(inputPathZip, finalZipName, sentPath, thumbPath, thumbName);

    }
}
