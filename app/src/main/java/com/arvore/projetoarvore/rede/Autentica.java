package com.arvore.projetoarvore.rede;

        import android.app.Activity;
        import android.content.Intent;
        import android.os.AsyncTask;
        import android.widget.TextView;

        import com.arvore.projetoarvore.SingleTon;
        import com.arvore.projetoarvore.telas.Login;
        import com.arvore.projetoarvore.telas.Termos;

        import org.json.JSONException;
        import org.json.JSONObject;

        import java.net.URL;

        import okhttp3.FormBody;
        import okhttp3.OkHttpClient;
        import okhttp3.Request;
        import okhttp3.RequestBody;
        import okhttp3.Response;

public class Autentica extends AsyncTask<String,Integer, Integer> { //<Parâmetros,Progresso,Resultado>
    SingleTon instance;
    TextView resultadoText;
    private Activity activity;
    String nome, telefone;
    public Autentica(TextView resultadoText, Activity activity) {
        this.resultadoText = resultadoText;
        this.activity = activity;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        resultadoText.setText("Logando...");
    }

    @Override
    protected Integer doInBackground(String... params) {
        instance = SingleTon.getInstance();
        nome = params[0];
        telefone = params[1];
        FormBody.Builder formBuilder = new FormBody.Builder()
                .add("login", params[0])
                .add("password", params[1]);
        RequestBody formBody = formBuilder.build();
        Request request = new Request.Builder()
                .url(instance.getServer() + "loginAndPasswordMatch")
                .post(formBody)
                .build();
        OkHttpClient okHttpClient = instance.getClient();
        try {
            Response response = okHttpClient.newCall(request).execute();
            if (!response.isSuccessful()) {
                System.out.println(response.toString());
                return (0);
            }
            JSONObject obj = new JSONObject(response.body().string());
            System.out.println(obj.toString());
            Integer res = obj.getInt("message");
            System.out.println("$$$$$$ Autentica.java: " + res);
            return res;
        } catch (Exception e) {
            e.printStackTrace();
            return (0);
        }
    }

    @Override
    protected void onPostExecute(Integer s) {
        super.onPostExecute(s);
        if (s == 0) {
            resultadoText.setText(s);
            CriaUser criaUser = new CriaUser(nome, telefone, resultadoText, activity);
            criaUser.execute();
        } else {
            instance.setId(s);
            activity.startActivity(new Intent(activity, Termos.class));
            activity.finish();
        }
    }
}