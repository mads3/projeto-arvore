package com.arvore.projetoarvore.rede;

import android.os.Environment;

import com.arvore.projetoarvore.SingleTon;
import com.arvore.projetoarvore.telasflow.Relatorio;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Envia {

    public void uploadZip(String zipPath, String zipName, String sentPath, String thumbPath, String thumbName) {
        SingleTon instance = SingleTon.getInstance();
        final String IP = instance.getServer();
        final String PATH = "fileupload";
        final String url = IP+PATH;
        final MediaType MEDIA_TYPE_ZIP = MediaType.parse("application/zip");

        File zip = new File(zipPath + "/" +zipName);
        OkHttpClient client = new OkHttpClient();

        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("file", zipName, RequestBody.create(MEDIA_TYPE_ZIP, zip))
                .build();
        System.out.println("url: " + url);
        Request request = new Request.Builder().url(url).post(requestBody).build();

        Response response = null;
        try {
            response = client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(response != null) {
            System.out.println("response.message(): " + response.message());
            System.out.println("response.toString(): " + response.toString());
        }
        File from = new File(thumbPath, thumbName);
        File to = new File(sentPath );
        System.out.println("FROM: ");
        System.out.println(from.getAbsolutePath());
        System.out.println("TO: ");
        System.out.println(to.getAbsolutePath());
        try {
            moveFile(from, to);
        } catch (IOException e) {
            e.printStackTrace();
        }




    }



    private void moveFile(File file, File dir) throws IOException {
        File newFile = new File(dir, file.getName());
        FileChannel outputChannel = null;
        FileChannel inputChannel = null;
        try {
            outputChannel = new FileOutputStream(newFile).getChannel();
            inputChannel = new FileInputStream(file).getChannel();
            inputChannel.transferTo(0, inputChannel.size(), outputChannel);
            inputChannel.close();
            file.delete();
        } finally {
            if (inputChannel != null) inputChannel.close();
            if (outputChannel != null) outputChannel.close();
        }

    }

}
