package com.arvore.projetoarvore.rede;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;

import com.arvore.projetoarvore.SingleTon;
import com.arvore.projetoarvore.telas.Termos;

import org.json.JSONObject;

import java.security.PublicKey;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class GetLogin extends AsyncTask<String, Integer, Integer> {

    SingleTon instance;
    String nome;
    private Activity activity;

    public GetLogin(String nome, Activity activity) {
        this.nome = nome;
        this.activity  = activity;

    }
    @Override
    protected Integer doInBackground(String... params) {
        instance = SingleTon.getInstance();
        FormBody.Builder formBuilder = new FormBody.Builder()
                .add("login", params[0]);
        RequestBody formBody = formBuilder.build();
        Request request = new Request.Builder()
                .url(instance.getServer() + "getUserIdbyLogin")
                .post(formBody)
                .build();
        OkHttpClient okHttpClient = instance.getClient();
        try {
            Response response = okHttpClient.newCall(request).execute();
            if(!response.isSuccessful()) {
                System.out.println(response.toString());
                return(-1 );
            }

            JSONObject obj = new JSONObject(response.message());
            Integer id = obj.getInt("message");
            System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ id json" +id +" " +obj);

            System.out.println(id);
            return id;
        } catch (Exception e) {
            e.printStackTrace();
            return(-1);
        }
    }

    @Override
    protected void onPostExecute(Integer id) {
        super.onPostExecute(id);
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$3");
        instance.setId(id);
        activity.startActivity(new Intent(activity, Termos.class));
        activity.finish();
    }
}
