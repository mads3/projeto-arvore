package com.arvore.projetoarvore.rede;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.TextView;

import com.arvore.projetoarvore.SingleTon;
import com.arvore.projetoarvore.telas.Login;
import com.arvore.projetoarvore.telas.Termos;

import org.json.JSONObject;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class CriaUser extends AsyncTask<String, Integer, Integer> { //<Parâmetros,Progresso,Resultado>

    String nome;
    String telefone;
    SingleTon instance;
    TextView resultadoText;
    private Activity activity;

    public CriaUser(String nome, String telefone, TextView resultadoText, Activity activity) {
        this.nome = nome;
        this.resultadoText = resultadoText;
        this.activity = activity;
        this.nome = nome;
        this.telefone = telefone;
    }

    @Override
    protected Integer doInBackground(String... strings) {
        instance = SingleTon.getInstance();
        FormBody.Builder formBuilder = new FormBody.Builder()
                .add("login", nome)
                .add("password", telefone)
                .add("nome", nome)
                .add("phone_number", telefone)
                .add("termo", "false");
        RequestBody formBody = formBuilder.build();
        Request request = new Request.Builder()
                .url(instance.getServer() + "insereUsuario")
                .post(formBody)
                .build();
        OkHttpClient okHttpClient = instance.getClient();
        try {
            Response response = okHttpClient.newCall(request).execute();
            if (!response.isSuccessful()) {
                System.out.println(response.toString());
                return (-2);
            }
            JSONObject obj = new JSONObject(response.body().toString());
            Integer id = obj.getInt("id");
            System.out.println("A RESPOSTA DO SERVIDOR FOI: " + id.toString());
            return id;
        } catch (Exception e) {
            e.printStackTrace();
            return (-2);
        }
    }

    @Override
    protected void onPostExecute(Integer id) {
        super.onPostExecute(id);
        System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$4");
        System.out.println("ID: " + id);
        if (id < 1) {
            System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$5");
            System.out.println("\nSenha incorreta\n\n");
            resultadoText.setText("Login ja existe");
            activity.startActivity(new Intent(activity, Login.class));
        } else {
            instance.setId(id);
            instance.setNome(nome);
            instance.setTelefone(telefone);
            instance.setId(id);
            activity.startActivity(new Intent(activity, Termos.class));
        }
    }
}

