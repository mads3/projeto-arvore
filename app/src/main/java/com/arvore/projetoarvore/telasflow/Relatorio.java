package com.arvore.projetoarvore.telasflow;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.NetworkType;
import androidx.work.Operation;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;
import androidx.work.WorkerParameters;

import com.arvore.projetoarvore.Processo;
import com.arvore.projetoarvore.R;
import com.arvore.projetoarvore.SingleTon;
import com.arvore.projetoarvore.background.TentaEnviar;
import com.arvore.projetoarvore.rede.Envia;
import com.arvore.projetoarvore.telas.Principal;
import com.arvore.projetoarvore.telas.SMS_Erro;
import com.arvore.projetoarvore.telas.SMS_Sucesso;
import com.arvore.projetoarvore.zip.Zippa;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Relatorio extends AppCompatActivity {

    Processo processo = Processo.getInstance();
    SingleTon singleTon = SingleTon.getInstance();
    TextView problemaView, gravidadeView;
    EditText enderecoText, especieText, frutoText, problemaText, adicionaisText;
    String inputPathZip, inputPathThumb ;
    SeekBar gravidadeBar;
    String timeStamp;
    String thumbPath;
    String finalZipName;
    String finalSentPath;
    String thumbName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relatorio);
        problemaView = (TextView)findViewById(R.id.problemaView);
        enderecoText = (EditText) findViewById(R.id.enderecoText);
        especieText = (EditText) findViewById(R.id.especieText);
        frutoText = (EditText) findViewById(R.id.frutoText);
        problemaText = (EditText) findViewById(R.id.problemaText);
        adicionaisText = (EditText) findViewById(R.id.adicionaisText);
        gravidadeBar = (SeekBar) findViewById(R.id.gravidadeBar);
        gravidadeView= (TextView)findViewById(R.id.gravidadeView);
        if(processo.getTipo() == 0){
            problemaView.setVisibility(View.INVISIBLE);
            problemaText.setVisibility(View.INVISIBLE);
            gravidadeView.setVisibility(View.INVISIBLE);
            gravidadeBar.setVisibility(View.INVISIBLE);
        }
        timeStamp  = processo.getTimestamp();
        inputPathZip  = getExternalFilesDir(Environment.DIRECTORY_MUSIC).getPath();
        inputPathThumb  = getExternalFilesDir(Environment.DIRECTORY_MOVIES).getPath();
        finalSentPath = getExternalFilesDir(Environment.DIRECTORY_PODCASTS).getPath();
        thumbPath = inputPathThumb;
        gravidadeBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                processo.setGravidade(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }

    public void zippa(){
        String[] s = new String[4];

        String jsonAbsolutePath = criaJsonFile();
        s[0] = processo.getInteiraPath();
        s[1] = processo.getCopaPath();
        s[2] = processo.getFolhaPath();
        s[3] = jsonAbsolutePath;

        // Em um futuro faz isso numa thread e coloca uma progressBar
        Zippa zipFile = new Zippa();
        thumbName = singleTon.getId() + "_" + timeStamp + ".jpg";
        finalZipName = timeStamp +".zip";
        zipFile.zip(s, inputPathZip + "/" + finalZipName );
        System.out.println(inputPathZip);
        System.out.println(finalZipName);
        Bitmap origiBit = BitmapFactory.decodeFile(processo.getInteiraPath());
        Bitmap newBit = Bitmap.createScaledBitmap(origiBit, 200, 200, true);
        FileOutputStream fo;
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        try {
            fo = new FileOutputStream(thumbPath + "/" + thumbName);         //////
            newBit.compress(Bitmap.CompressFormat.JPEG,70 , outStream);
            fo.write(outStream.toByteArray());
            fo.close();
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public void enviar(View view) {

        processo.setEnderecoArvore(enderecoText.getText().toString());
        processo.setEspecieArvore(especieText.getText().toString());
        processo.setFruto(frutoText.getText().toString());
        processo.setProblemaConstatado(problemaText.getText().toString());
        processo.setInformacoesAdicionais(adicionaisText.getText().toString());
        zippa();


        PeriodicWorkRequest workRequest;
        Data parametros = new Data.Builder()
                .putString("inputPathZip", inputPathZip) //
                .putString("finalZipName", finalZipName)
                .putString("finalSentPath", finalSentPath)
                .putString("thumbPath", inputPathThumb)
                .putString("thumbName", thumbName)
                .build();
        Constraints constraints = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.UNMETERED)  // https://developer.android.com/reference/androidx/work/Constraints.Builder
                .build();

        final WorkManager mWorkManager = WorkManager.getInstance();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            workRequest = new PeriodicWorkRequest.Builder(TentaEnviar.class, 15, TimeUnit.MINUTES)  // intervalo minimo de 15 minutos segundo especificacao developer.android.com/reference/androidx/work/PeriodicWorkRequest
                    .setConstraints(constraints)
                    .setInputData(parametros)
                    .build();
            mWorkManager.enqueue(workRequest);
            if(true){
                startActivity(new Intent(this, SMS_Sucesso.class));
            } else {
                startActivity(new Intent(this, SMS_Erro.class));
            }
            System.out.println("Saindo do if workRequest");
        }


        Toast.makeText(this, "Enviando", Toast.LENGTH_SHORT);
        try {Thread.sleep(500);} catch (InterruptedException e) {e.printStackTrace();}
              startActivity(new Intent(this, SMS_Sucesso.class));

        //teste

        finish();
        // Insere as informacoes dentro, envia
    }

/*    public static void envia_pela_rede_sincrono(String inputPathZip, String finalZipName, String sentPath) {
        Envia envia = new Envia();
        try {
            String test = inputPathZip + "/";
            envia.uploadZip(inputPathZip, finalZipName, sentPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
*/
    public String criaJsonFile() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("InteiraLat", processo.getInteiraLat().toString());
            jsonObject.put("InteiraLon", processo.getInteiraLon().toString());
            jsonObject.put("CopaLat", processo.getCopaLat().toString());
            jsonObject.put("CopaLon", processo.getCopaLon().toString());
            jsonObject.put("FolhaLat", processo.getFolhaLat().toString());
            jsonObject.put("FolhaLon", processo.getFolhaLon().toString());
            jsonObject.put("Tipo", processo.getTipo().toString());          // 0: cadastro  1: denuncia
            jsonObject.put("EnderecoArvore", processo.getEnderecoArvore());
            jsonObject.put("EspecieArvore", processo.getEspecieArvore());
            jsonObject.put("Fruto", processo.getFruto());
            jsonObject.put("Gravidade", processo.getGravidade().toString());
            jsonObject.put("ProblemaConstatado", processo.getProblemaConstatado());
            jsonObject.put("InformacoesAdicionais", processo.getInformacoesAdicionais());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String timeStamp = processo.getTimestamp();
        String fileName = "infos" + timeStamp + "_";

        File infoJson = new File(this.getFilesDir(), fileName + ".json");
        FileWriter fileWriter = null;
        BufferedWriter bufferedWriter = null;

        System.out.println(jsonObject.toString());
        if(!infoJson.exists()) {
            try {
                infoJson.createNewFile();
                fileWriter = new FileWriter(infoJson.getAbsoluteFile());
                bufferedWriter = new BufferedWriter(fileWriter);
                bufferedWriter.write(jsonObject.toString());
                bufferedWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return infoJson.getAbsolutePath();
    }


    private File createImageFile(String path) throws IOException {
        // Create an image file name
        File storageDir = getExternalFilesDir(path);
        File image = File.createTempFile(
                finalZipName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        // Save a file: path for use with ACTION_VIEW intents
        //thumbPath = image.getAbsolutePath();
        return image;
    }
}
