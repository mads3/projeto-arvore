package com.arvore.projetoarvore.telasflow;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.arvore.projetoarvore.Processo;
import com.arvore.projetoarvore.R;
import com.arvore.projetoarvore.SingleTon;

import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import pub.devrel.easypermissions.EasyPermissions;

public class Inteira extends AppCompatActivity {

    private static final int FINE_LOCATION_REQUEST_CODE = 42;
    private static final int FINE_LOCATION_PERMISSION_CODE = 43;
    private static final int CAMERA_REQUEST_CODE = 1450;
    private static final int CAMERA_PERMISSION_CODE = 1460;
    private static final int ACCESS_WIFI_STATE= 1460;
    ImageView inteiraView;
    Button okButton, retryButton, pulaButton;
    private String mCurrentPhotoPath;
    Processo processo = Processo.getInstance();
    SingleTon instance;
    File photoFile;
    Uri photoURI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inteira);
        instance = SingleTon.getInstance();
        inteiraView = (ImageView) findViewById(R.id.inteiraView);
        okButton = (Button)findViewById(R.id.buttonOk);
        retryButton = (Button)findViewById(R.id.buttonRetry);
        pulaButton = (Button)findViewById(R.id.pulaButton);
        if (!EasyPermissions.hasPermissions(Inteira.this, Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_WIFI_STATE)) {
            EasyPermissions.requestPermissions(Inteira.this, getString(R.string.permission_text), CAMERA_PERMISSION_CODE
                    , Manifest.permission.CAMERA, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_WIFI_STATE);
        }
        configurarServico();
    }

    public void onOk(View view) {
        launchCamera();
        retryButton.setVisibility(View.VISIBLE);
        okButton.setText("Envia!");
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processo.setInteiraPath(mCurrentPhotoPath);
                avancaParaProxima();
            }
        });
    }

    public void pulaButton(View view) {
        processo.setInteiraLon(-1.0);
        processo.setInteiraLat(-1.0);
        processo.setInteiraPath(Processo.getBlockPath(getApplicationContext()));
        avancaParaProxima();
    }

    private void avancaParaProxima() {
 /*      // processo.setInteira(fotoBit);
        System.out.println("mCurrentPhotoPath: " + mCurrentPhotoPath);
        System.out.println("Uri.getPath(): " + photoURI.getPath());
        System.out.println("Uri.toString(): " + photoURI.toString());
        System.out.println("Uri.getEncodedPath(): " + photoURI.getEncodedPath());
        System.out.println("photoFile.getAbsolutePath: " + photoFile.getAbsolutePath());
        try {
            System.out.println("photoFile.getCanonicalPath: " + photoFile.getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        */
        startActivity(new Intent(this, Copa.class));
    }

    private void launchCamera() {

        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        // Create the File where the photo should go
        if(photoFile == null) {
            try {
                photoFile = createImageFile();
            }
            catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        // Continue only if the File was successfully created
        if (photoFile != null) {
            photoURI = FileProvider.getUriForFile(this,
                    "com.arvore.ProjetoArvore",
                    photoFile);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            //Start the camera application
            startActivityForResult(cameraIntent, CAMERA_REQUEST_CODE);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST_CODE) {
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inSampleSize = 4;
            Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
            inteiraView.setImageBitmap(bitmap);
        }
    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = processo.getTimestamp();
        String imageFileName = instance.getId().toString() + "_Inteira_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }


    // Localizacao daqui pra baixo:


    public void configurarServico(){
        try {
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            LocationListener locationListener = new LocationListener() {
                public void onLocationChanged(Location location) {
                    atualizar(location);
                }

                public void onStatusChanged(String provider, int status, Bundle extras) { }

                public void onProviderEnabled(String provider) { }

                public void onProviderDisabled(String provider) { }
            };
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        }catch(SecurityException ex){
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void atualizar(Location location)
    {
        Double latPoint = location.getLatitude();
        Double lngPoint = location.getLongitude();
        processo.setInteiraLat(latPoint);
        processo.setInteiraLon(lngPoint);
        //    localizacao.setText("LAT: "+latPoint.toString()+" LON: "+lngPoint.toString());

    }




}

