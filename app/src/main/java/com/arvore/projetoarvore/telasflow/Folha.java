package com.arvore.projetoarvore.telasflow;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.arvore.projetoarvore.Processo;
import com.arvore.projetoarvore.R;
import com.arvore.projetoarvore.SingleTon;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import pub.devrel.easypermissions.EasyPermissions;

public class Folha extends AppCompatActivity {

    private static final int CAMERA_REQUEST_CODE = 1450;
    private static final int CAMERA_PERMISSION_CODE = 1460;
    ImageView folhaView;
    Button okButton, retryButton, pulaButton;
    private String mCurrentPhotoPath;
    Processo processo = Processo.getInstance();
    SingleTon instance;
    File photoFile;
    Uri photoURI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_folha);
        instance = SingleTon.getInstance();
        folhaView = (ImageView)findViewById(R.id.folhaView);
        okButton = (Button)findViewById(R.id.buttonOk);
        retryButton = (Button)findViewById(R.id.buttonRetry);
        pulaButton = (Button)findViewById(R.id.pulaButton);
        if (EasyPermissions.hasPermissions(Folha.this, Manifest.permission.CAMERA)) {
            EasyPermissions.requestPermissions(Folha.this, getString(R.string.permission_text), CAMERA_PERMISSION_CODE, Manifest.permission.CAMERA);
        }
        configurarServico();
    }


    public void onOk(View view) {
        launchCamera();
        retryButton.setVisibility(View.VISIBLE);
        okButton.setText("Envia!");
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processo.setFolhaPath(mCurrentPhotoPath);
                avancaParaProxima();
            }
        });
    }

    public void pulaButton(View view) {
        processo.setFolhaLon(-1.0);
        processo.setFolhaLat(-1.0);
        processo.setFolhaPath(Processo.getBlockPath(getApplicationContext()));
        avancaParaProxima();
    }

    private void avancaParaProxima() {
        // processo.setInteira(fotoBit);
        startActivity(new Intent(this, Relatorio.class));
    }

    private void launchCamera() {

        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        // Create the File where the photo should go
        if(photoFile == null) {
            try {
                photoFile = createImageFile();
            }
            catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        // Continue only if the File was successfully created
        if (photoFile != null) {
            photoURI = FileProvider.getUriForFile(this,
                    "com.arvore.ProjetoArvore",
                    photoFile);
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            //Start the camera application
            startActivityForResult(cameraIntent, CAMERA_REQUEST_CODE);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Preview the image captured by the camera
        if (requestCode == CAMERA_REQUEST_CODE) {
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inSampleSize = 4;
            Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
            folhaView.setImageBitmap(bitmap);
        }
    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = processo.getTimestamp();
        String imageFileName = instance.getId().toString() +  "_Folha_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }


    public void configurarServico(){
        try {
            LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            LocationListener locationListener = new LocationListener() {
                public void onLocationChanged(Location location) {
                    atualizar(location);
                }

                public void onStatusChanged(String provider, int status, Bundle extras) { }

                public void onProviderEnabled(String provider) { }

                public void onProviderDisabled(String provider) { }
            };
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        }catch(SecurityException ex){
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    public void atualizar(Location location)
    {
        Double latPoint = location.getLatitude();
        Double lngPoint = location.getLongitude();
        processo.setFolhaLat(latPoint);
        processo.setFolhaLon(lngPoint);

        // localizacao.setText("LAT: "+latPoint.toString()+" LON: "+lngPoint.toString());

    }

}
