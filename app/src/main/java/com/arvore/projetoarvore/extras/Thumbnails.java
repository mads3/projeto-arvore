package com.arvore.projetoarvore.extras;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.arvore.projetoarvore.SingleTon;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Thumbnails {


    public static Bitmap[] generateBitmaps(String[] thumbPaths) {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inSampleSize = 4;
        ArrayList<Bitmap> tmp = new ArrayList<>();
        for(int i = 0; i < thumbPaths.length; i++) {
            if (thumbPaths[i] != null) {
               // thumbs[i] = null;
                tmp.add(BitmapFactory.decodeFile(thumbPaths[i], bmOptions));
               // thumbs[i] = BitmapFactory.decodeFile(thumbPaths[i], bmOptions);
            }
        }
        Bitmap[] thumbs = new Bitmap[tmp.size()];
        for(int j =0; j < tmp.size(); j++) {
            thumbs[j] = tmp.get(j);
        }
        return thumbs;
    }

    public static String[] getFilenamesStringFromFolder(String inputPath) {
        SingleTon instance = SingleTon.getInstance();
        File directory = new File(inputPath);
        File[] files = directory.listFiles();
        String filesGetName, idToString;
        ArrayList <String> temp = new ArrayList();
        for(int i = 0; i < files.length; i++) {
            filesGetName = files[i].getName();
            idToString = instance.getId().toString();
            if(filesGetName.startsWith(idToString)){
                temp.add(filesGetName);
               // filesList[i] = files[i].getName();
            }
        }
        String[] filesList = new String[temp.size()];
        for(int j = 0; j < temp.size(); j++) {
            if (temp.get(j) != null) {
                filesList[j] = temp.get(j);
            }
        }
        return (filesList);
    }

    public static String[] getCanonicalFilepathStringFromFolder(String inputPath) {
        SingleTon instance = SingleTon.getInstance();
        File directory = new File(inputPath);
        File[] files = directory.listFiles();
        String filesGetName, idToString; Boolean xy;
        ArrayList <String> temp = new ArrayList();
        for(int i = 0; i < files.length; i++) {
            xy = files[i].getName().contains("/" + instance.getId().toString() + "_");
            filesGetName = files[i].getAbsolutePath();
            idToString = "/" + instance.getId().toString() + "_";
                if(filesGetName.contains(idToString)) {
                    temp.add(filesGetName);
                    //filesList[i] = files[i].getAbsolutePath();

            }
        }
        String[] filesList = new String[temp.size()];

        for(int j = 0; j < temp.size(); j++) {
            filesList[j] = temp.get(j);
        }
        return (filesList);
    }
}

