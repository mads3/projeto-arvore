package com.arvore.projetoarvore.telas;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.arvore.projetoarvore.R;

import java.io.IOException;

public class ListCell extends ArrayAdapter<String> {
    private final Activity context;
    private  String[] infos;
    private Bitmap[] imageId;

    public ListCell(Activity context, String[] infos, Bitmap[] imageId) {
        super(context, R.layout.list_cell, infos);
        this.context = context;
        this.imageId = imageId;
        this.infos = infos;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_cell, null, true);
        TextView textView = (TextView) rowView.findViewById(R.id.text);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.img);
        textView.setText(infos[position]);
        imageView.setImageBitmap(imageId[position]);
        return rowView;
    }

}