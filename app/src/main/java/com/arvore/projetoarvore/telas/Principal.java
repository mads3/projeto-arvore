package com.arvore.projetoarvore.telas;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.arvore.projetoarvore.Processo;
import com.arvore.projetoarvore.R;
import com.arvore.projetoarvore.SingleTon;
import com.arvore.projetoarvore.adapters.TabsAdapter;
import com.arvore.projetoarvore.extras.SlidingTabLayout;
import com.arvore.projetoarvore.fragments.FragmentAguardando;
import com.arvore.projetoarvore.fragments.FragmentEnviados;
import com.arvore.projetoarvore.telasflow.Inteira;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;


public class Principal extends AppCompatActivity {

    SingleTon INSTANCE = SingleTon.getInstance();
    TextView nomeView;
    ListCell adapter;
    ListView list;
    GoogleSignInClient mGoogleSignInClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Inicializa ViewPager e carrega as tabs
        initViewPager();

        nomeView = (TextView)findViewById(R.id.nomeView);
        nomeView.setText(INSTANCE.getNome());

        FloatingActionButton fab = findViewById(R.id.logoutButton);
        fab.setOnClickListener((view) -> {
            logout();
        });
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(Principal.this, gso);

    }

    public void fazRegistro(View view) {
        Processo processo = Processo.getInstance();
        processo.zeraProcesso();
        processo.setTipo(0);
        startActivity(new Intent(this, Inteira.class));

    }

    public void fazDenuncia(View view) {
        Processo processo = Processo.getInstance();
        processo.zeraProcesso();
        processo.setTipo(1);
        startActivity(new Intent(this, Inteira.class));
    }


    private void logout(){
        mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(Principal.this, "Deslogado com sucesso", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(Principal.this, Login.class));
                        finish();
                    }
                });
    }


    private void initViewPager() {
        // Instancia o ViewPager a partir do resource adicionado no layout activity_main.xml
        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);

        if (viewPager != null) {
            setupViewPager(viewPager);
        }
        // Da mesma forma o SlidingTabLayout, também incluso no layout activity_main.xml
        SlidingTabLayout tabLayout = (SlidingTabLayout) findViewById(R.id.tabs);
        //noinspection ConstantConditions
        tabLayout.setSelectedIndicatorColors(Color.WHITE);
        tabLayout.setTextColorResId(R.color.tabs_text_color);

        tabLayout.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
        tabLayout.setViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        TabsAdapter adapter = new TabsAdapter(getSupportFragmentManager());
        adapter.addFragment(new FragmentEnviados(), "Enviados");
        adapter.addFragment(new FragmentAguardando(), "Aguardando envio");
        viewPager.setAdapter(adapter);
    }









}