package com.arvore.projetoarvore.telas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.arvore.projetoarvore.R;


public class SMS_Sucesso extends AppCompatActivity {

    TextView SMSText;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms__sucesso);

        try {Thread.sleep(3000);} catch (InterruptedException e) {e.printStackTrace();}
        startActivity(new Intent(this, Principal.class));
    }
}
