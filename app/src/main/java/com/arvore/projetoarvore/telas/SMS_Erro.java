package com.arvore.projetoarvore.telas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.arvore.projetoarvore.R;

public class SMS_Erro extends AppCompatActivity {

    TextView SMSText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms__erro);
        SMSText = (TextView)findViewById(R.id.resultadoView);
        SMSText.setText("ERRO");
    }
}
