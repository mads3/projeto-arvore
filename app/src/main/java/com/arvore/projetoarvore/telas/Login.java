package com.arvore.projetoarvore.telas;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.arvore.projetoarvore.R;
import com.arvore.projetoarvore.SingleTon;
import com.arvore.projetoarvore.extras.Hash;
import com.arvore.projetoarvore.rede.Autentica;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Login extends AppCompatActivity {


    Button login;
    EditText nome, telefone;
    SingleTon instance;
    TextView resutadoText;
    int RC_SIGN_IN = 0;
    SignInButton signInButton;
    GoogleSignInClient mGoogleSignInClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        signInButton = findViewById(R.id.sign_in_button);
        instance = SingleTon.getInstance();
        instance.setId(3);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        signInButton.setOnClickListener((view) -> {
            runGoogle();
        });

      //  facebook = (Button) findViewById(R.id.facebookButton);
        login = (Button)findViewById(R.id.logarButton);
        nome = (EditText) findViewById(R.id.nomeEdit);
        telefone = (EditText)findViewById(R.id.telefoneEdit);

        instance = SingleTon.getInstance();
        resutadoText = (TextView)findViewById(R.id.resultadoView);

        System.out.println("AQUI ESTA O SINGLETON: " + instance.getNome() + instance.getTelefone() + instance.getId());
    }

    public void runGoogle() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            instance.setNome(account.getGivenName());

            String googleLoginId = account.getId();
            Integer subId = Integer.parseInt(googleLoginId.substring(5, 8));
            instance.setId(subId);
            startActivity(new Intent(Login.this, Termos.class));
        } catch (ApiException e) {
            Log.w("Google Sign-in error", "signInResult:failed code = " + e.getStatusCode());
            resutadoText.setText("Tente novamente");
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        if (account != null) {
            instance.setNome(account.getGivenName());
            startActivity(new Intent(Login.this, Termos.class));
        }
    }

    public void autentica(String login,  String senha) {
        instance.setNome(nome.getText().toString());
        Activity activity = this;
        Autentica autentica = new Autentica(resutadoText, activity);
        autentica.execute(login, senha);
    }

    public void onLoginSuccess() {
        instance.setNome(nome.getText().toString());
        instance.setTelefone(telefone.getText().toString());
        startActivity(new Intent(this, Termos.class));
        finish();
    }

    public void onLoginFail() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(),"Login falhou",Toast.LENGTH_SHORT);
            }
        });
    }

    public void runLogar(View view) {
        String hashPass = Hash.md5(nome.getText().toString());
        instance.setTelefone(hashPass);
        autentica(nome.getText().toString(), instance.getTelefone());
    }

/*    private String getMyPhoneNO() {
        TelephonyManager tMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String mPhoneNumber = tMgr.getLine1Number();
        return mPhoneNumber;
    }
*/
}


