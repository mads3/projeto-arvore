package com.arvore.projetoarvore.telas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.arvore.projetoarvore.R;
import com.arvore.projetoarvore.SingleTon;
import com.arvore.projetoarvore.rede.GetLogin;

public class Termos extends AppCompatActivity {

    Button aceito;
    CheckBox checkBox;
    SingleTon INSTANCE = SingleTon.getInstance();
    TextView nomeView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_termos);
        aceito = (Button)findViewById(R.id.aceitoButton);
        checkBox = (CheckBox)findViewById(R.id.checkBox);
        nomeView = (TextView)findViewById(R.id.nomeView);
        nomeView.setText(INSTANCE.getNome());
    }

    public void clicaAceita(View view) {

        if(checkBox.isChecked()) {
            INSTANCE.setAceitouOsTermos(true);
            startActivity(new Intent(this, Principal.class));
            finish();
        }
    }

}
