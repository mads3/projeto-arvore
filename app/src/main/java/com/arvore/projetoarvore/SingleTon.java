package com.arvore.projetoarvore;

import okhttp3.OkHttpClient;

// Trocar pela classe AccountManager
public class SingleTon {
    private static SingleTon ourInstance = new SingleTon();

    private Integer id;
    private String nome;
    private String telefone;
    private Boolean aceitouOsTermos;
    OkHttpClient client = new OkHttpClient();
    private String server = "http://200.236.3.108:3000/";
    // private String server = "http://192.168.0.8:3000/";

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public OkHttpClient getClient() { return client; }

    public static SingleTon getInstance() {
        if(ourInstance == null) {
            ourInstance = new SingleTon();
        }
        return ourInstance;
    }

    private SingleTon() {
    }


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Boolean getAceitouOsTermos() {
        return aceitouOsTermos;
    }

    public void setAceitouOsTermos(Boolean aceitouOsTermos) {
        this.aceitouOsTermos = aceitouOsTermos;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


}
