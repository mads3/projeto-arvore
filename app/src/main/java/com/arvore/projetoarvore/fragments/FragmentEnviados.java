package com.arvore.projetoarvore.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.arvore.projetoarvore.R;
import com.arvore.projetoarvore.extras.Thumbnails;
import com.arvore.projetoarvore.telas.ListCell;
import com.arvore.projetoarvore.telas.Principal;

import java.io.File;

public class FragmentEnviados extends Fragment {

    ListView list;
    ListCell adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        String[] thumbsNames = Thumbnails.getFilenamesStringFromFolder(container.getContext().getExternalFilesDir(Environment.DIRECTORY_PODCASTS).getPath());
        String[] thumbsPaths = Thumbnails.getCanonicalFilepathStringFromFolder(container.getContext().getExternalFilesDir(Environment.DIRECTORY_PODCASTS).getPath());
        Bitmap[] thumbsArray = Thumbnails.generateBitmaps(thumbsPaths);
        View v = inflater.inflate(R.layout.fragment_enviados, container, false);
        adapter = new ListCell(getActivity(), thumbsNames, thumbsArray);

        list = (ListView)v.findViewById(R.id.list);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            }
        });
        return v;
    }



    String[] infos = {
            "teste",
            "um",
            "dois",
    } ;

    Integer[] imageId = {
            R.drawable.common_google_signin_btn_icon_light,
            R.drawable.common_google_signin_btn_icon_light,
            R.drawable.common_google_signin_btn_icon_light,
    };

}
