package com.arvore.projetoarvore;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;

import com.arvore.projetoarvore.telas.Principal;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Processo {
    private static Processo ourInstance = new Processo();

    private String enderecoArvore;
    private String especieArvore;
    private String fruto;
    private Integer gravidade;
    private String problemaConstatado;
    private String informacoesAdicionais;
    private String inteiraPath;
    private Double inteiraLat;
    private Double inteiraLon;
    private Double folhaLat;
    private Double folhaLon;
    private Double copaLat;
    private Double copaLon;
    private String copaPath;
    private String folhaPath;
    private Integer tipo; // 0: cadastro  1: denuncia

    private String timestamp;

    public static Processo getOurInstance() {
        return ourInstance;
    }

    public static void setOurInstance(Processo ourInstance) {
        Processo.ourInstance = ourInstance;
    }

    public void zeraProcesso() {
        this.enderecoArvore = "";
        this.especieArvore = "";
        this.fruto = "";
        this.gravidade = 1;
        this.problemaConstatado = "";
        this.informacoesAdicionais = "";
        this.inteiraLat = -1.0;
        this.inteiraLon = -1.0;
        this.folhaLat = -1.0;
        this.folhaLon = -1.0;
        this.copaLat = -1.0;
        this.copaLon = -1.0;
        this.inteiraPath = "";
        this.copaPath = "";
        this.folhaPath = "";
        this.tipo = -1; // 0: cadastro  1: denuncia
        setTimestamp();
    }


    public Double getInteiraLat() {
        return inteiraLat;
    }

    public void setInteiraLat(Double inteiraLat) {
        this.inteiraLat = inteiraLat;
    }

    public Double getInteiraLon() {
        return inteiraLon;
    }

    public void setInteiraLon(Double inteiraLon) {
        this.inteiraLon = inteiraLon;
    }

    public Double getFolhaLat() {
        return folhaLat;
    }

    public void setFolhaLat(Double folhaLat) {
        this.folhaLat = folhaLat;
    }

    public Double getFolhaLon() {
        return folhaLon;
    }

    public void setFolhaLon(Double folhaLon) {
        this.folhaLon = folhaLon;
    }

    public Double getCopaLat() {
        return copaLat;
    }

    public void setCopaLat(Double copaLat) {
        this.copaLat = copaLat;
    }

    public Double getCopaLon() {
        return copaLon;
    }

    public void setCopaLon(Double copaLon) {
        this.copaLon = copaLon;
    }

    public Integer getGravidade() {
        return gravidade;
    }

    public void setGravidade(Integer gravidade) {
        this.gravidade = gravidade;
    }

    public String getInteiraPath() {
        return inteiraPath;
    }

    public void setInteiraPath(String inteiraPath) {
        this.inteiraPath = inteiraPath;
    }

    public String getCopaPath() {
        return copaPath;
    }

    public void setCopaPath(String copaPath) {
        this.copaPath = copaPath;
    }

    public String getFolhaPath() {
        return folhaPath;
    }

    public void setFolhaPath(String folhaPath) {
        this.folhaPath = folhaPath;
    }

    public static Processo getInstance() {
        if(ourInstance == null) {
            ourInstance = new Processo();
        }
        return ourInstance;
    }

    private Processo() {
    }


    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp() {
        this.timestamp =  new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
    }

    public String getEnderecoArvore() {
        return enderecoArvore;
    }

    public void setEnderecoArvore(String enderecoArvore) {
        this.enderecoArvore = enderecoArvore;
    }

    public String getEspecieArvore() {
        return especieArvore;
    }

    public void setEspecieArvore(String especieArvore) {
        this.especieArvore = especieArvore;
    }

    public String getFruto() {
        return fruto;
    }

    public void setFruto(String fruto) {
        this.fruto = fruto;
    }

    public String getProblemaConstatado() {
        return problemaConstatado;
    }

    public void setProblemaConstatado(String problemaConstatado) {
        this.problemaConstatado = problemaConstatado;
    }

    public String getInformacoesAdicionais() {
        return informacoesAdicionais;
    }

    public void setInformacoesAdicionais(String informacoesAdicionais) {
        this.informacoesAdicionais = informacoesAdicionais;
    }

    public Integer getTipo() {
        return tipo;
    }

    public void setTipo(Integer tipo) {
        this.tipo = tipo;
    }


    public static String getBlockPath(Context ctx) {

        Drawable drawable = ctx.getDrawable(R.drawable.block);
        String blockPath = ctx.getExternalFilesDir(Environment.DIRECTORY_NOTIFICATIONS).getPath() + "/" + "block.png";
        if (drawable != null) {
            Bitmap bmp = ((BitmapDrawable) drawable).getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            FileOutputStream fo;
            try {
                fo = new FileOutputStream(blockPath);
                bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                fo.write(stream.toByteArray());
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return blockPath;
    }

}
